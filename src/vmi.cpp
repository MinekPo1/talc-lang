#include <map>
#include <set>
#include <fmt/format.h>
#include <exception>
#include <stdexcept>

#include "atomizer.cpp"
#include "util.cpp"

#define uint unsigned int
#define CHECK_EOF(what) \
	if (!get().is) \
		return Error( \
			fmt::format("Expected {}, got EOF",what), \
			get(-1).value.pos \
		)

using std::map;
using std::set;

enum VMIT{
	I_NOP,
	I_PULL,
	I_PUSH,
	I_PEEK,
	I_POP,
	I_ADD,
	I_SUB,
	I_MUL,
	I_DIV,

	I_CALL,
	I_SET,
	I_CLR,
	I_TCHECK,

	I_JUMP,
	I_JIF,  // no, not GIF
};

struct VMI {
	VMIT type;
	uint arg;
	bool ABReg;
	Pos position;
};

struct Procedure {
	vector<uint> args;
	uint iptr;
};

struct Type {
	map<string, uint> names;
};

struct Name {
	Optional<Procedure> proc_ptr;
	uint type;
};

struct VMIGen;

struct Expression {
	virtual void push(VMIGen &gen, bool ABReg);
};

struct Variable: Expression {
	uint name;
};

struct PeekStack: Expression {
	uint depth;
};

struct PopStack: Expression {
	uint depth;
};

struct VMIGen {
	vector<Atom> atoms;
	vector<VMI> instructions;
	map<string,vector<Atom>> defered_procs;
	map<string,uint> name_map;
	vector<Name> names;
	vector<Type> types = {{{}}, {{}}, {{}}, {{}}};
	// 0 - int
	// 1 - float
	// 2 - string
	// 3 - proc
	vector<uint> lables;
	set<uint> needed_procs;
	string ns_prefix;

	int index = 0;

	inline Optional<Atom> get(int cindex = 0);

	Result<> processInclude();

	Result<> prescanProcedure();
	Result<> prescanInclude();
	Result<> prescanTypeDef();
	Result<> prescanRoot();
	Result<> prescan();

	Result<> scanBlock();
	Result<> scanCall();
	
	Result<Expression> parseExpression();
};

inline Optional<Atom> VMIGen::get(int cindex) {
	if (cindex + index >= atoms.size())
		return None;
	return atoms[index + cindex];
}

Result<> VMIGen::prescanProcedure(){
	index ++;
	CHECK_EOF("procedure name");
	Atom atom = get().value;
	if (atom.type != NAME and atom.value != ":")
		return Error(
			fmt::format("Expected procedure name, got `{}`",atom.value),
			atom.pos
		);
	string name;
	
	if (atom.value == ":")
		name = fmt::format("anon@{}",atom.pos);
	else {
		name = atom.value;
		index ++;
	}

	debug(fmt::format("found procedure {}",name));

	defered_procs[name];

	CHECK_EOF("a colon");
	atom = get().value;
	if (atom.value != ":")
		return Error(
			fmt::format("Expected a colon, got `{}`", atom.value),
			atom.pos
		);
	index ++;
	CHECK_EOF("a block");
	atom = get().value;
	if (atom.type != BLOCK_START)
		return Error(
			fmt::format("Expected a block, got `{}`", atom.value),
			atom.pos
		);
	index ++;
	atom = get().value;
	if (atom.type == BLOCK_END) {
		return Error("empty block lul",atom.pos);
	}
	do {
		defered_procs[name].push_back(atom);
		index ++;
		atom = get().value;
	}
	while(atom.type != BLOCK_END);

	return None;
}

Result<> VMIGen::processInclude() {
	Atom atom = get().value;
	if (atom.type != NAME)
		return Error(
			fmt::format("Expected a import name, got `{}`",atom.value),
			atom.pos
		);
	return None;
}

Result<> VMIGen::prescanInclude() {
	index ++;
	CHECK_EOF("import name");
	Atom atom = get().value;

	if (atom.type != NAME and atom.type != BLOCK_START)
		return Error(
			fmt::format("Expected a block, got `{}`", atom.value),
			atom.pos
		);
	
	if (atom.type == BLOCK_START) {
		index ++;
		CHECK_EOF("import name");
		atom = get().value;
		do {
			auto r = processInclude();
			if (!r.success) return r.error;
			index ++;
			CHECK_EOF("import name");
			atom = get().value;
		} while(atom.type != BLOCK_END);
		return None;
	}

	return processInclude();
}

Result<> VMIGen::prescanRoot(){
	// calling function guarrantees that get() is not None
	Atom atom = get().value;
	if (atom.type != KEYWORD)
		return Error(
			fmt::format("Unexpected `{}` in root context",atom.value),
			atom.pos
		);

	if (atom.value == "proc")
		return prescanProcedure();

	if (atom.value == "with")
		return prescanInclude();

	return Error(
		fmt::format("Unexpected `{}` in root context",atom.value),
		atom.pos
	);
}

Result<> VMIGen::prescan() {
	Optional<Atom> oatom;
	while ((oatom = get()).is) {
		debug(fmt::format("atom: `{}` (@{})",oatom.value.value,oatom.value.pos));

		auto res = prescanRoot();
		if (!res.success) return res.error;

		index ++;
	}
	return None;
}

Result<vector<VMI>> generateVMI(vector<Atom> atoms){
	debug("generating VMI");
	debug(fmt::format("got {} atoms",atoms.size()));

	VMIGen gen;
	gen.atoms = atoms;
	
	debug("prescanning...");

	auto rpre = gen.prescan();
	if (!rpre.success) return rpre.error;

	debug("procedures:");

	for (auto proc: gen.defered_procs) {
		debug(fmt::format(
			"  {}",
			proc.first
		));
	}

	return gen.instructions;
}
