#include <string>
#include <map>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <ostream>

#include "atomizer.cpp"
#include "vmi.cpp"
#include "util.cpp"

using std::string;
using std::map;

enum Target{
	T_UKN,
	/* T_X86,
	T_X86_64,
	T_ARM,
	T_ARM_64, */
	T_ATM,
	T_VMI,
	T_NO_IMPL,
};

Target T_X86    = T_NO_IMPL;
Target T_X86_64 = T_NO_IMPL;
Target T_ARM    = T_NO_IMPL;
Target T_ARM_64 = T_NO_IMPL;

enum Option {
	O_Target,
	O_Input,
	O_Output,
};
enum Flag {
	F_Verbose,
	F_Silent,
	F_Yaml,
	F_Help,
	F_Target,
};

const map<string, Target> TargetNames {
	{"X86",    T_X86},
	{"X86_64", T_X86_64},
	{"ARM",    T_ARM},
	{"ARM_64", T_ARM_64},
	{"ATM",    T_ATM},
	{"VMI",    T_VMI}
};

const map<string, Flag> flags {
	{"-h",       F_Help},
	{"--help",   F_Help},
	{"-V",       F_Verbose},
	{"-verbose", F_Verbose},
	{"-S",       F_Silent},
	{"--silent", F_Silent},
	{"-y",       F_Yaml},
	{"--yaml",   F_Yaml},
	{"-t",       F_Target},
	{"--target", F_Target},
	{"--targets",F_Target},
};

const map<string, Option> options {
	{"-o",       O_Output},
	{"--output", O_Output},
	{"-t",       O_Target},
	{"--target", O_Target},
	{"--targets",O_Target},
};

const Option ioptions[] {
	O_Input,
	O_Output,
};

Target target;
string input = "";
string output = "-";
bool   yaml;

string action = "build";

// from
// https://stackoverflow.com/a/116220
std::string slurp(std::ifstream& in) {
    std::ostringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

void processOpt(Option option, string value) {
	switch (option) {
	case O_Input:
		input = value;
		break;
	case O_Output:
		output = value;
		break;
	case O_Target:
		if (TargetNames.find(value) != TargetNames.end())
			target = TargetNames.at(value);
		else {
			printf("Unknown target %s.\nTry ctalc --targets",value.c_str());
			action = "error";
		}
		break;
	}
}

void processFlag(Flag flag) {
	switch (flag) {
		case F_Help:
			action = "help";
			break;
		case F_Silent:
			verbose --;
			break;
		case F_Verbose:
			verbose ++;
			break;
		case F_Yaml:
			yaml = true;
			break;
		case F_Target:
			action = "targets";
			break;
	}
}

int printHelp(){
	printf("Usage:\n");
	printf("  ctalc <options> [<input> [<output>]]\n\n");
	printf("\tinput                 Where to take the code from.\n");
	printf("\t-o, --output, output  Where to output the result.\n");
	printf("\t                      If set to `-` (default) stdout will be used.\n");
	printf("\t-t, --target[s]       Target to compile to. If used without a value,\n");
	printf("\t                      list of targets will be outputed.\n");
	printf("\t-y, --yaml            Format output in yaml. Only valid for ATM and AST targets\n");
	printf("\t-V, --verbose         Increase verbosety. Can be used multiple times.\n");
	printf("\t-S, --silent          Decrease verbosety. ------------||------------\n");

	return 0;
}

Optional<string> parseOpt(string &arg) {
	string value = arg.substr(arg.find("=")+1);
	arg = arg.substr(0,arg.find("="));
	
	if (options.find(arg) == options.end()){
		printf("unknown option %s\ntry ctalc --help\n", arg.c_str());
		action = "error";
		return Optional<string>();
	}

	return Optional<string>(value);
}

bool parseLFlag(string arg){
	debug(arg);
	if (flags.find(arg) == flags.end()){
		printf("unknown flag %s\ntry ctalc --help\n", arg.c_str());
		action = "error";
		return true;
	}
	debug(fmt::format("{}",flags.at(arg)));
	return false;
}

bool parseSFlags(string arg){
	for (int j = 1; j < arg.size(); j ++){
		string flag = "-" + string({arg[j]});
		if (flags.find(flag) == flags.end()){
			fmt::print("unknown flag {}\ntry ctalc --help\n", flag);
			action = "error";
			return true;
		}
		processFlag(flags.at(flag));
		continue;
	}

	return false;
}

void parseArgs(int argc, char **argv) {
	int count_iop = 0;
	for (int i = 1; i < argc; i ++) {
		Optional<string> r;
		string arg = argv[i];
		if (arg[0] != '-' or arg == "-"){
			processOpt(ioptions[count_iop],arg);
			count_iop ++;
			continue;
		}
		if (arg.find('=') != -1) {
			if (not (r = parseOpt(arg)).is) return;
			processOpt(options.at(arg),r.value);
			continue;
		}
		if (arg[1] == '-'){
			if (parseLFlag(arg)) return;
			processFlag(flags.at(arg));
			continue;
		}
		if(parseSFlags(arg)) return;
	}
}

void pprintAtom(Atom atom){
	fmt::print(
		"  {} {}:{}:{}  \t{}\n",
		ATypeName[atom.type].c_str(),
		atom.pos.source.c_str(),
		atom.pos.lin, atom.pos.col,
		atom.value.c_str()
	);
}

void yprintAtom(Atom atom) {
	fmt::print("- type: {}\n", ATypeName[atom.type]);
	fmt::print("  pos: {{file: {}, line: {}, col: {}}}\n",
		atom.pos.source, atom.pos.lin, atom.pos.col
	);
	fmt::print("  value: >-\n    {}\n",atom.value);
}

void build_atm(vector<Atom> atoms) {
	auto printAtom = yaml ? yprintAtom : pprintAtom;
	fmt::print("atoms:\n");
	for (Atom atom : atoms)
		printAtom(atom);
}

int build(){
	if (target == T_NO_IMPL){
		printf("Target not implemented\nTry ctalc --targets");
		return 1;
	}
	if (target == T_UKN){
		printf("Please specify a valid target\nTry ctalc --targets");
		return 1;
	}

	std::ifstream file;
	file.open(input);
	string code = slurp(file);
	// printf("code:\n%s\n",code.c_str());
	auto ratm = atomize(code,input);
	
	debug("Done atomizing.");
	
	if (target == T_ATM) {
		auto rb_atm = ratm >>= build_atm;
		// TODO: display errors
		return rb_atm.success;
	}

	auto rVMI = ratm >>= generateVMI;

	if (not rVMI.success){
		dispError(rVMI.error);
		return 1;
	}

	return 0;
}

int targets(){
	for (auto target: TargetNames){
		if (target.second != T_NO_IMPL)
			printf("%s\n",target.first.c_str());
		else
			printf("%s\t(unimplemented)\n", target.first.c_str());
	}
	return 0;
}

int main(int argc, char **argv) {
	parseArgs(argc, argv);
	if (action == "error")
		return 1;
	if (action == "targets") {
		return targets();
	}
	if (action == "help" or input == "")
		return printHelp();
	if (action == "build")
		return build();
	printf("Did not do anything.\n");
	return 1;
}
