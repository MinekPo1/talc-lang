#pragma once

#include <string>
#include <stdio.h>
#include <fmt/format.h>
using std::string;

struct Pos {
	int lin;
	int col;
	string source;

	Pos() {
		this->lin = 0;
		this->col = 0;
		this->source = "unknown";
	}

	Pos(int lin, int col, string source) {
		this->lin = lin;
		this->col = col;
		this->source = source;
	}

	operator string();
};

Pos::operator string(){
	return fmt::format("{}:{}:{}",source,lin,col);
}

template<>
struct fmt::formatter<Pos>{

	constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
		auto it = ctx.begin();

		return it;
	}

	template<typename FormatContext>
	auto format(const Pos& pos,FormatContext& ctx) const -> decltype(ctx.out()) {
		return fmt::format_to(ctx.out(),"{}:{}:{}", pos.source,pos.lin,pos.col);
	}
};

struct Error {
	string message;
	Pos pos;

	Error (){
	}

	Error (string message, Pos pos) {
		this->message = message;
		this->pos = pos;
	}
};

void dispError(Error error) {
	// TODO: make this more fancy
	fmt::print("error: {} @{}\n", error.message, error.pos);
}

int verbose = 0;

void debug(string msg) {
	if (verbose >= 2)
		printf("DEBUG: %s\n",msg.c_str());
}

void log(string msg) {
	if (verbose >= 1)
		printf("LOG: %s\n",msg.c_str());
}

void info(string msg) {
	if (verbose >= 0)
		printf("INFO: %s\n",msg.c_str());
}

void error(string msg) {
	if (verbose >= -1)
		printf("ERROR: %s\n",msg.c_str());
}

void fatal(string msg) {
	if (verbose >= -1)
		printf("FATAL: %s\n",msg.c_str());
}

template<typename T>
class Optional{
	public:
		T value;
		bool is;
		Optional(T value);
		Optional();
};

template<typename T>
Optional<T>::Optional(T value) {
	this->is    = true;
	this->value = value;
}

template<typename T>
Optional<T>::Optional(){
	this->is    = false;
}

class NoneType {
	private:
		static NoneType instance;
		NoneType(){}
	public:
		static NoneType getInstance();
		
		template<typename T>
		operator Optional<T>();
};

NoneType NoneType::getInstance() {
	return instance;
}

NoneType None = NoneType::getInstance();

template<typename T>
NoneType::operator Optional<T>() {
	return Optional<T>();
}

template<typename T = NoneType>
class Result {
	public:
	bool success;
	T value;
	Error error;

	Result(T i_value) : value(i_value), success(true) {}
	Result(Error i_error) : error(i_error), success(false) {}
	
	template<typename R>
	Result<R> bind(Result<R> bound(T));

	template<typename R>
	Result<R> map(R bound(T));
	Result<> map(void bound(T));

	// metaprogramming: >>=
	template<typename R>
	inline Result<R> operator>>=(Result<R> bound(T));
	template<typename R>
	inline Result<R> operator>>=(R bound(T));
	inline Result<> operator>>=(void bound(T));
};

template<>
Result<>::Result(Error i_error) : value(None), error(i_error), success(false) {} 

template<typename T>
template<typename R>
Result<R> Result<T>::bind(Result<R> bound(T)) {
	if (!success) {
		return Result<R>(error);
	}
	return bound(value);
}

template<typename T>
template<typename R>
inline Result<R> Result<T>::operator>>=(Result<R> bound(T)) {
	return Result<T>::bind(bound);
}

template<typename T>
template<typename R>
Result<R> Result<T>::map(R bound(T)) {
	if (!success) {
		return Result<R>(error);
	}
	return Result<R>(bound(value));
}

template<typename T>
Result<> Result<T>::map(void bound(T)) {
	if (!success)
		return Result<>(error);
	bound(value);
	return Result<>(None);
}

template<typename T>
template<typename R>
inline Result<R> Result<T>::operator>>=(R bound(T)) {
	return Result<T>::map(bound);
}

template<typename T>
inline Result<> Result<T>::operator>>=(void bound(T)) {
	return Result<T>::map(bound);
}
