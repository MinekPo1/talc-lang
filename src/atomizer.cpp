#pragma once

#include <vector>
#include <string>
#include <stack>
#include <unistd.h>
#include <fmt/format.h>

#include "util.cpp"
using std::string;
using std::vector;
using std::stack;

enum AType {
	UNKNOWN,
	NAME,
	NUMBER,
	KEYWORD,
	STRING,
	OPERATOR,
	BLOCK_START,
	BLOCK_END,
};

string ATypeName[] {
	"UNKNOWN    ",
	"NAME       ",
	"NUMBER     ",
	"KEYWORD    ",
	"STRING     ",
	"OPERATOR   ",
	"BLOCK_START",
	"BLOCK_END  ",
};

struct Atom {
	AType type;
	string value;
	Pos pos;

	Atom(AType type, string value, Pos pos) {
		this->type = type;
		this->value = value;
		this->pos = pos;
	}

	Atom(string value, Pos pos) {
		this->type = UNKNOWN;
		this->value = value;
		this->pos = pos;
	}

	Atom(){}
};

struct Indent {
	int tabs;
	int spaces;
};

vector<string> splitlines(string code) {
	vector <string> out;
	string cur;
	// 0 -> empty  1 -> code  2 -> comment  3 -> code + comment
	char state = 0;

	for (char i: code) {
		if (i == '\n') {
			if (state & 1)
				out.push_back(cur);
			else
				out.push_back("");
			cur = "";
			state = 0;
			continue;
		}
		if (state == 0 and string(" \t;").find(i) == -1)
			state = 1;
		if (i == ';' and cur[cur.size()-1] == ';' and state < 2){
			state += 2;
			cur = cur.substr(0,cur.size()-1);
			continue;
		}
		if (state < 2)
			cur += i;
	}
	
	if (cur != "")
		out.push_back(cur);
	
	return out;
}

bool processBreakChars(
		vector<Atom> &out, int col, int &bcol, char i, string &cur, bool &in_string,
		int nline, string source
	){
	if (string(" \t\n@^!+-*/\":=<>").find(i) == -1)
		return false;
	
	// make f-strings one atom
	if (cur == "f" and i == '"') {
		in_string = true;
		cur = "f\"";
		return true;
	}
	if (not cur.empty()) {
		// if i is a -, dont break if part of a name.
		if (i == '-' and string("0123456789").find(cur[0]) == -1){
			cur += '-';
			return true;
		}
		out.push_back(Atom(cur,Pos(nline,bcol,source)));
		cur = "";
		bcol = col + 1;
	}
	if (string(" \t\n").find(i) != -1)
		return true; 
	// merge ==,<=,>=,!=
	if (
		    (i == '=')
		and out[out.size()-1].pos.col == col - 1
	) {
		string &p = out[out.size()-1].value;
		if (string("!<>=").find(p) != -1){
			p += i;
			return true;
		}
	}
	if (string(" \t\n\"").find(i) == -1){
		out.push_back(Atom(string({i}),Pos(nline,col,source)));
	}
	else
		cur = "";
	bcol = col + 1;
	if (i == '"'){
		in_string = true;
		cur = "\"";
	}
	return true;
	
}

void processString(
		string &cur, bool in_string, vector<Atom> &out, char i,
		int &bcol, int col, int nline, string source
	){
	cur += i;
	if (i == '"' and cur[cur.size() -1] != '\\'){
		in_string = false;
		out.push_back(Atom(STRING,cur,Pos(nline,bcol,source)));
		cur = "";
		bcol = col + 1;
	}
}

vector<Atom> split(string code, int nline, string source) {
	vector<Atom> out;
	string cur;
	int col = 0;
	int bcol = 1;
	bool in_string = false;
	
	for (char i: code) {
		col ++;
		if (in_string) {
			processString(cur,in_string,out,i,bcol,col,nline,source);
			continue;
		}
		if (processBreakChars(out,col,bcol,i,cur,in_string,nline,source))
			continue;
		cur += i;
	}
	
	if (not cur.empty())
		out.push_back(Atom(cur,Pos(nline,bcol,source)));

	return out;
}

Indent getIndent(string line) {
	Indent out = {0,0};

	for (char i: line) {
		if (i == ' ')
			out.spaces += 1;
		else if (i == '\t')
			out.tabs += 1;
		else
			break;
	}
	return out;
}

Optional<Error> manageIndent(stack<Indent> &indents, string line, int nline, vector<Atom> &atoms, string source) {
	debug("getting indent...");
	Indent indent = getIndent(line);

	debug(fmt::format("current: {} {}", indent.tabs, indent.spaces));
	debug("checking...");

	if (
		    indents.top().spaces != indent.spaces
		and indents.top().tabs   != indent.tabs
	)
		return Optional<Error>(Error(
			"Could not compare indent to previous line.",
			Pos(nline, 0, source)
		));
	if (
		    indents.top().spaces < indent.spaces
		or  indents.top().tabs   < indent.tabs
	) {
		atoms.push_back(Atom(BLOCK_START,"",Pos(nline,0,source)));
		indents.push(indent);
		debug("block start");
		return Optional<Error>();
	}
	while (
		    indents.top().spaces > indent.spaces
		or  indents.top().tabs   > indent.tabs
	) {
		atoms.push_back(Atom(BLOCK_END,"",Pos(nline,0,source)));
		indents.pop();
		debug("block end");
	}
	return Optional<Error>();
}

vector<string> keywords = {
	"proc",
	"var",
	"for",
	"while",
	"if",
	":",".",",",
	"=","!",
	"with",
	"->",
	"^","@",
};

vector<string> operators = {
	"+","-",
	"*","/",
	"and",
	"or",
	"xor",
	"not",
	"==","!=",
	"<","<=",
	">",">=",
};

Atom fillType(Atom atom) {
	if (atom.type != UNKNOWN)
		return atom;

	if (string("0123456789").find(atom.value[0]) != -1){
		atom.type = NUMBER;
		return atom;
	}
	
	if (std::find(operators.begin(),operators.end(),atom.value) != operators.end()) {
		atom.type = OPERATOR;
		return atom;
	}

	if (std::find(keywords.begin(), keywords.end(), atom.value) != keywords.end()) {
		atom.type = KEYWORD;
		return atom;
	}

	if (atom.value[0] == '"' or atom.value.substr(0,2) == "f\"") {
		atom.type = STRING;
		return atom;
	}

	atom.type = NAME;
	return atom;
}

Result<vector<Atom>> atomize(string code, string source) {
	vector<Atom> out;
	stack<Indent> indents;

	indents.push({0,0});

	int nline = 0;
	debug("splitting lines...");
	for (string line: splitlines(code)) {
		nline ++;
		if (line == "")
			continue;
		debug(fmt::format("atomizing line {} in {}...",nline,source));
		debug(fmt::format("    | {}",line));

		Optional<Error> o;
		if ((o = manageIndent(indents, line, nline, out, source)).is)
			return o.value;
		debug("indent ok...");
	
		for (Atom atom: split(line, nline, source))
			out.push_back(fillType(atom));
	}

	manageIndent(indents,"",nline,out,source);

	return out;
}


