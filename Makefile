CPPFLAGS :=  -std=c++11 -lfmt

cpp_sources = $(shell find src/*.cpp)
cpp_objects = $(patsubst src/%.cpp, build/%.o, $(cpp_sources))

build:
	mkdir build

ctalc: build/ctalc

.PHONY: ctalc

build/ctalc: build/entry.o build
	$(CXX) $(CPPFLAGS) -o build/ctalc build/entry.o

depend: .depend

.depend: $(cpp_sources)
	$(RM) ./.depend
	@echo $^
	$(foreach source,$(cpp_sources),$(CXX) $(CPPFLAGS) -MM $(source) -MT $(patsubst src/%.cpp, build/%.o, $(source))>>./.depend;)

build/%.o: src/%.cpp build
	$(CXX) $(CPPFLAGS) -c $< -o $@

clean:
	$(RM) -r ./build

distclean: clean
	$(RM) *~ .depend

include .depend
